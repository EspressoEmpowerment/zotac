// Check wishlist for ADD ALL TO CART Button                               
// If detected click it. Once the product has been added                   
// launch a paypal express start link                                           
var pagetitle = String(document.title);
if (pagetitle.includes("Wishlist")) {
  console.log(document.title);
  var elementExists = document.getElementsByClassName("button btn-add");
  if (elementExists.length > 0) {
      console.log("ADD ALL TO CART");
      document.getElementsByClassName("button btn-add")[0].click();
  } else {
      console.log("Out of Stock");
  }
  // If a product has been added launch a paypal express start link             
  var url = document.location.href;
    if (url.includes("wishlist_id")) {
      console.log("Opening paypal");
  window.open("https://us.zotacstore.com/us/paypal/express/start");
  }
}

