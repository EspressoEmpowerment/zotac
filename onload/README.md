## **AUTO CART VIA ATC BUTTON**

If a 30 series product page is detected on zotac the script will try to detect if you already have something in your cart.  If your cart has
something in it this script will not run. The idea is to attempt to prevent multiple cards from getting in a cart due to zotac’s new 1 card per 
household every 2 weeks policy.  If an add to cart button exists and the card is in stock it will attempt to click it.

***
## **LAUNCH PAYPAL EXPRESS WINDOW FROM CART**

This script will check to see if the page loaded is the shopping cart
page.  If it is and it sees an item come in stock it will launch a
paypal express window.  It uses localStorage to make sure the link
only gets launched once per session.  Make sure you set the paypal
localStorage flag to 0 when starting to hunt for a card. On first run
at the console enter the following:

localStorage.setItem('paypal', '0');

Updated cart checking logic, updated OOS detection, works as of 5/28.
Tested during zotac cart but can’t buy event.

***
## **PLAY SOUND FILE IF ADD ALL TO CART APPEARS ON WISHLIST**

This script will detect if the page is a wishlist page and if it sees
that ADD ALL TO CART button it will play an audio file from a URL.
Should still work as of 5/27

***
## **AUTO LOGIN**

If the login page is detected, attempt to log in.  Make sure you
update with your credentials.  Doesn’t help much now that captcha has
been implemented.  Also is letting you stay logged in a lot longer
without any captcha rechecks.

Thanks to this zotac guide for most of this auto login script.
https://pastebin.com/0T16hkVc

***
## **SHOW INVENTORY AND ATC URL IN CONSOLE**

If a 30 series product page is detected on zotac the script will
display the inventory available as well as the CART URL in the
console.  The CART URL will look something like this:

http://www.zotacstore.com/us/checkout/cart/add/uenc/aHR4cHM7Ly93d3cuem90YWNzdG9yZS5jb20vdXNvgWNjZXNzb3JpZXM,/product/1423/form_key2l15V5PazrZ5mPDc/

This should still work as of the 5/27 update.  However note the actual
add to cart URL itself will not work if clicked.  Also not sure if the
stock reporting mechanism is the same.  But should be mostly harmless
even if it fails.

***
## **CART FROM WISHLIST**

This script will detect if the page is a wishlist page and if it sees
that ADD ALL TO CART button it will try to click it.  With zotac
limiting orders to two households per week you may want to use caution
as depending on your wishlist it would be easy to cart multiple cards.
I now also have scripts to auto-cart from individual project projects
so in most cases I would probably recommend using them to cart instead
of trying to cart from wishlist.  Should still work as of 5/27 update

To test, log into your zotac account in an incognito window and add an
in stock item to your wishlist.  This should trigger the code if it is
working on the next refresh.  You can also view your console and it
should be printing out messages you can see on the console.

***
## **CART FROM WISHLIST AND LAUNCH PAYPAL EXPRESS START LINK**

This script builds upon the “cart from the wishlist” script.  After
hitting the ADD ALL TO CART button the script will attempt to launch a
paypal express start script in a new tab.  This was the start of a
fully automated auto purchase script but with zotac under load during
a drop I don’t think it’s feasible.  Not sure there is much utility to
this script but leaving it up anyway.  Updated URL and should still
work after the 5/27 update.

***
## **AUTO CART VIA ATC LINK ANY 30 SERIES CARD BY VISITING ITS PRODUCT PAGE**

If a 30 series product page is detected on zotac the script will try
to detect if you already have something in your cart.  If your cart
has something in it this script will not run.  The idea is to attempt
to prevent multiple cards from getting in a cart due to zotac’s new 1
card per household every 2 weeks policy.  By default this script will
launch one ATC link per each product page that loads.  If your cart is
empty and there is stock then an ATC link will be launched into a new
tab.  The ATC link will look something like this:

http://www.zotacstore.com/us/checkout/cart/add/uenc/aHR4cHM7Ly93d3cuem90YWNzdG9yZS5jb20vdXNvgWNjZXNzb3JpZXM,/product/1423/form_key2l15V5PazrZ5mPDc/

You could then copy and paste that link as many times as you would
like into other tabs.  Also, you can modify the default value by
editing the tabcount variable in the javascript code.  For example, if
you set tabcount to 5 this script would launch 5 ATC links.  I think
zotac will prevent you from carting 2 of the same 30 series item but
I’m not sure on this.  Someone let me know if you have experience with
this.

This WILL NOT WORK as of the 5/27 update. Links no longer have
form_key in them.
***

